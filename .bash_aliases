# adding aliases for terminal control

alias q=exit
alias clc=clear
alias cs='clear;ls'
alias ca='clear;la'
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'

# Some ls aliases
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias t=time

# Git aliases
alias sts='git status'
alias com='git commit -m'
alias clone='git clone'
alias all='git add .'
alias pu='git push'
alias pl='git pull'
alias stash='git stash'
alias branchl='git branch -a'
alias branch='git branch'
alias checkout='git checkout'

# Program aliases
alias vimrc='vim ~/.vimrc'
alias bashrc='vim ~/.bashrc'
alias bashaliases='vim ~/.bash_aliases'
alias loadbash='source ~/.bashrc'
alias i3config='vim ~/.config/i3/config'
alias roficonfig='vim ~/.config/rofi/config.rasi'

# Poweroff
alias poweroff='sudo poweroff'
alias reboot='sudo reboot now'

# Game alias
alias d2="wine /mnt/d25e00be-f7c1-47bf-8603-aa7a3d513309/D2/Diablo_II/D2SE.exe"
alias hero="wine /home/simon/.wine/drive_c/Program\ Files\ \(x86\)/Hero\ Editor/Hero\ Editor.exe"
alias civ="wine /home/simon/.wine/drive_c/Program\ Files\ \(x86\)/R.G.\ Mechanics/Sid\ Meier\'s\ Civilization\ 5/CivilizationV.exe"
alias shc="wine /home/simon/.wine/drive_c/Program\ Files\ \(x86\)/FireFly\ Studios/Stronghold\ Crusader\ HD\ Enhanced\ Edition/Stronghold\ Crusader.exe"
alias sc2="wine /home/simon/.wine/drive_c/Program\ Files/StarCraft\ II/StarCraft\ II\ Offline.exe"


# Adding alias for python3
alias py=python3

# alias for Julia
alias jl=julia

# Git Dotfile Repo
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# QCAD Alias
alias qcad='cd /home/simon/opt/qcad-3.28.1-pro-linux-x86_64/; ./qcad; cd /home/simon/'
