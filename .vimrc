filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround' 
Plugin 'tpope/vim-commentary' 
" Plugin 'numirias/semshi'
Plugin 'Valloric/YouCompleteMe'
" Plugin 'vim-latex/vim-latex'
Plugin 'morhetz/gruvbox'
Plugin 'JuliaEditorSupport/julia-vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
"
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
set runtimepath+=~/.vim_runtime

nmap <CR> o<Esc>

" colors
set bg=dark " Setting dark mode 
colorscheme gruvbox
syntax enable " enable syntax processing 

" tabs
set tabstop=4 " # of visual spaces per tab set to 4
set softtabstop=4 " # of tabs when editing
set expandtab " now tabs are spaces

" UI config
set number " show line numbers
set relativenumber
set showcmd " show command in bottom bar
set cursorline " adds cursor line
filetype indent on " load filetype specific indent files
set wildmenu " autocomplete from command menu
set lazyredraw " avoids redrawing when doing macros 
set showmatch " shows [] ()
set incsearch " search as characters are entered 
set hlsearch  "hilights searches

" turn off search highlight
nnoremap <Esc><Esc> :nohlsearch<CR>

" Movement
nnoremap B ^
nnoremap E $

" Leader shortcuts
let mapleader="," " leader is comma instead of \

" save session
nnoremap <leader>s :mksession!<CR>

"rm trailing whitespace
"called on buffer write in autogroup above
function! <SID>StripTrailingWhitespaces()
    " Save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

try
source ~/.vim_runtime/my_configs.vim
catch
endtry

"set fold method
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2

" add autocomment 1 line
nnoremap <C-i> ^i# <Esc>
nnoremap <S-tab> ^xx<Esc>

" rm highlighting
nmap <esc><esc> :noh<return>

" disable visual bell, disable audio bell
set novisualbell
set noerrorbells visualbell t_vb=

" tpope plugins 
autocmd FileType python setlocal commentstring=#\ %s
autocmd Filetype tex setlocal tabstop=4
autocmd Filetype tex setlocal commentstring=\%\ %s

" Vim-Latex stuff
filetype indent on
set sw=4 

set nocompatible " be iMproved, required
set nocp

set t_ku=OA
set t_kd=OB
set t_kr=OC
set t_kl=OD

" Vim-Julia auto Latex-to-Unicode as space
let g:latex_to_unicode_auto = 1

" Delete file \ automatically when exiting vim
autocmd VimLeave * !rm \

